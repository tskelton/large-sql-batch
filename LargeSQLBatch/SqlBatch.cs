﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Configuration;

namespace LargeSQLBatch
{
    public partial class SqlBatch : Form
    {
        public SqlBatch()
        {
            InitializeComponent();
        }

        private void folderBrowserDialog1_HelpRequest(object sender, EventArgs e)
        {

        }

        private void SqlBatch_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            connection.Open();

            DirectoryInfo dir = new DirectoryInfo(textBox1.Text);

            button1.Enabled = false;
            button2.Enabled = false;

            foreach(var file in dir.GetFiles("*.sql"))
            {
                var reader = new StreamReader(file.OpenRead());
                var lineCount = File.ReadLines(file.FullName).Count();
                progressBar1.Minimum = 0;
                progressBar1.Maximum = lineCount;
                string script = string.Empty,line = string.Empty;
                int count = 0;
                label4.Text = file.Name;
                var regex = new Regex(@"'([^']*')");
                var regQuote = new Regex("'");

                while((line = reader.ReadLine()) != null)
                {
                    script += line;

                    if(line.EndsWith(";"))
                    {
                        try
                        {
                            SqlCommand command = new SqlCommand(script, connection);
                            command.ExecuteNonQuery();
                            label5.Text = count.ToString();
                            script = string.Empty;
                        }
                        catch(SqlException sqle)
                        {
                            DialogResult result = MessageBox.Show(sqle.Message + "\r\nWould you like to continue?", "Sql Exception", MessageBoxButtons.YesNo);
                            if(result == DialogResult.No)
                            {
                                break;
                            }
                            script = string.Empty;
                        }
                    }
                    ++count;
                    progressBar1.Value = count;
                    Application.DoEvents();
                }
                reader.Close();
            }
            connection.Close();
            MessageBox.Show("Process Complete!", "Result", MessageBoxButtons.OK);

            button1.Enabled = true;
            button2.Enabled = true;
        }
    }
}
